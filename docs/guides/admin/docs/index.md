Opencast Administration Guide
=============================

Welcome to the Matterhorn Universe! Matterhorn is an open-source enterprise
level lecture recording system. The core of the system delivers functionality
for scheduling, media encoding, editing and content delivery. For lecture
capture, Matterhorn provides capture agent software and third party appliances
are available. An awesome community provides new features and support.


The Software
------------

Matterhorn contains everything you need for scheduling captures, trimming,
captioning, and conversion of output media to several formats and our engage
components.  The core can be deployed on one (all-in-one deployment) or many
(distributed deployment) Linux servers so your Matterhorn installation can grow
with the needs of your university.


Release Documentation
---------------------

The Matterhorn Release Documentation is the official Matterhorn documentation
for each release. It contains:

 - [Release Notes](release.notes.md)
 - [Installation Guides](installation/index.md)
 - [Configuration Guides](configuration/index.md)
    - [Basic Configuration](configuration/basic.md)
    - [Database Configuration](configuration/database.md)
    - [Workflow Configuration](configuration/workflow.md)
    - [Encoding Configuration](configuration/encoding.md)
    - [more...](configuration)
 - [Module Documentation](modules/index.md)
    - [Atom and RSS Feed](modules/atomrss.md)
    - [Media Module](modules/mediamodule.configuration.md)
    - [Text Extraction](modules/textextraction.md)
    - [Video Segmentation](modules/videosegmentation.md)
    - [YouTube Publication](modules/youtubepublication.md)
    - [more...](modules/index.md)
 - [Upgrade Instructions](upgrade/index.md)


Further Documentation
---------------------

Apart from these official documentation, further guides and tips can be found
in the [Matterhorn Adopter Wiki](https://opencast.jira.com/wiki), as well as on
the mailing lists, the IRC channel and the regular meetings.
